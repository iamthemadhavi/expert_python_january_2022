#simple dictionary
person = {
        "name" : "Yash",
        "age"  : 19,
        "city" : "Nagpur"
        }
print(person)

#for adding element
#person["gender"]="male"
#print(person)

#for updating element
#person.update({"age":23})
#print(person)


#for changing element
#person["age"] = 21
#print(person)

