#keyword argument
def my_func(x,y,z):
    print(f"value of x is {x}\nvalue of y is {y}\nvalue of z is {z}")

my_func(x=1,y=2,z=3)


#default parameter value
def my_func(city="Nagpur"):
    print("I am from " + city)

my_func("Mumbai")
my_func("Pune")
my_func()


#addtion of 2 num using recursive function
def addition(x,y):
    if(y==0):
        return(x)
    else:
        return(1+addition(x,y-1))

print(addition(10,5))

#to print hello world
def my_function():
    print("hello world")

my_function()

#addtion of 2 num
def addOfTwo(x,y):
    z = x+y
    print(z)

addOfTwo(2,2)


#mean of 3 num
def meanOfThree(x,y,z):
    sum = x+y+z
    mean = int(sum/3)
    print(f"mean of {x},{y},{z} is {mean}")

meanOfThree(2,3,4)

#recursive function
def fact(x):
    if x==0:   #base case
        return 1

    else:       #recursive case
        return x*fact(x-1)
print(fact(4))

