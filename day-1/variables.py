'''
my_name_is ..... snake case
myNameIs......camel case
MyNameIs.....pascal case
'''

#simple addition of 2 num....dynamically
num1 = int(input("Enter any value for num1: "))
num2 = int(input("Enter any value for num2: "))
#num1 = 5
#num2 = 2
sum = num1 + num2

#to print in a format of 5+2=7
print(f"{num1} + {num2} = {sum}")
