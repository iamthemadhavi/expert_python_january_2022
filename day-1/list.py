
fruits = ["apple","banana","cherry"]
print(fruits)

#to check datatype
print(type(fruits))

#to access specific element
print(fruits[1])

#loop through list
fruits = ["apple","banana","cherry"]
for x in fruits:
    print(x)
