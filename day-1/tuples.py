
thisTuple = ("apple","banana","cherry","kiwi")
#print(thisTuple)

#to check datatype
print(type(thisTuple))

#to access specific elemnts in tuple
print(thisTuple[1])

#looping through tuple
for x in thisTuple:
    print(x)
