#init function
class person:
    def __init__(self,name,age):
        self.name = name
        self.age = age

    def intro(self):
        print(f"Hello my name is {self.name}. My age is {self.age}.")

p1 = person("Madhavi",23)
person.intro(p1)

p2 = person("Prachi",20)
person.intro(p2)



#task to perfom create class of circle and find area and circumference
class circle:
    def __init__(self,r):
        self.radius = r

    def area(self):
        print(f"area of circle is {3.14 * self.radius * self.radius}")

    def circumference(self):
        print(f"circumference of circle is {2 * 3.14 * self.radius}")


r = int(input("Enter radius of circle: "))

cobj = circle(r)
cobj.area()
cobj.circumference()

#for checking datatype
class person:
    def intro(self):
        print("hello")
p1 = person()
person.intro(p1)

print(type(p1))

