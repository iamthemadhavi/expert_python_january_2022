
from flask import Flask, jsonify, request, render_template
app = Flask(__name__)  

@app.route('/show', methods=['GET'])
def hello_world():
    return "hello world"

if __name__ == "__main__":
    app.run(debug=True);    #it will generate port number
