x = {"firstName":"Yash", "lastName":"Rajurkar"}
#print(type(x))

'''
nested dictionary
x = {
      key:{value},
      key:{value},
      key:{value}
      }
'''

x = {
    "person1":
    {"name":"minakshi",
     "age":23,
     "city":"pune"},
     
    "person2":
    {"name":"sakshi",
     "age":24,
     "city":"nagpur"}
}
print(type(x))