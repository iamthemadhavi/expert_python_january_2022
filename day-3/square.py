#program for square

from flask import Flask, jsonify, request
#Cross Origin Resource Sharing (CORS).This package exposes a Flask extension 
#CORS support on all routes, for all origins and methods.
from flask_cors import CORS	
import pymysql
app = Flask(__name__)
cors = CORS(app)

@app.route('/sqr', methods=['GET'])
def getSqr():
    num1 = int(request.args.get('num1'));
    return f"Square of {num1} is {num1 * num1}"
  

if __name__ == "__main__":
    app.run(debug=True);
