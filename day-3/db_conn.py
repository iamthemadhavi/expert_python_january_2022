from flask import Flask, jsonify, request
#Cross Origin Resource Sharing (CORS).This package exposes a Flask extension  
#CORS support on all routes, for all origins and methods.
from flask_cors import CORS
import pymysql
app = Flask(__name__)
cors = CORS(app)


@app.route('/users', methods=['GET'])
def get_users():
    # To connect MySQL database
    conn = pymysql.connect(host='sql6.freesqldatabase.com', user='sql6468115', password = '5TzhdHvzpL', db='sql6468115')

    cur = conn.cursor()
    cur.execute("SELECT * FROM 1st_year")
    output = cur.fetchall()

    print(type(output)); 

    for rec in output:
        print(rec);

    # To close the connection
    conn.close()

    return jsonify(output);
    
if __name__ == "__main__":  #it will generate port number
    app.run(debug=True);
