#multilevel inheritance

#base class
class grandfather:

    def __init__(self,grandfathername):
        self.grandfathername = grandfathername
        
#intermediate class
class father(grandfather):

     def __init__(self,fathername,grandfathername):
        self.fathername = fathername
        
        #invoking grandfathername class
        grandfather.__init__(self,grandfathername)

#derive class
class son(father):

    def __init__(self,sonname,fathername,grandfathername):
        self.sonname = sonname
    
    #invoking father class
        father.__init__(self,fathername,grandfathername)
    
   
    def print_name(self):
    
        print("grandfather name is:", self.grandfathername)
        print("father name is:",self.fathername)
        print("son name is :",self.sonname)
        
        
        
s1 = son("prince","rampal","lal mani")
s1.print_name()