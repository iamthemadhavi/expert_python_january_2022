import pymysql

#show records
def showrecord():
    conn = pymysql.connect(host = "localhost",user = "root", password = "", db = "student");
    
    cur = conn.cursor();
    cur.execute("SELECT * FROM `1st_year`");
    
    output = cur.fetchall()     #to collect data
    for x in output:
        print(x);
        print();
        
#showrecord()


#insert data
def insert():
     conn = pymysql.connect(host = "localhost",user = "root", password = "", db = "student");
     
     #to ask user
     name = input("Enter name: ")
     sec = input("Enter section: ")
     city = input("Enter city: ")
     
     #sql
     insert_sql = "INSERT INTO `1st_year` (`roll_no`, `name`, `section`, `city`) VALUES (NULL, '"+name+"', '"+sec+"', '"+city+"')";
     cur = conn.cursor();
     cur.execute(insert_sql);
     
     conn.commit()
     print(cur.rowcount,"record is inserted")
     
#insert()

#update record
def update():
     conn = pymysql.connect(host = "localhost",user = "root", password = "", db = "student");
     
     #to ask user
     roll_no = int(input("Enter roll number:"))
     name = input("Enter name: ")
     sec = input("Enter section: ")
     city = input("Enter city: ")
     
     update_sql = "UPDATE `1st_year` SET `section` = '"+sec+"', `name` = '"+name+"', `city` = '"+city+"' WHERE `1st_year`.`roll_no` = '"+str(roll_no)+"';";
     cur = conn.cursor();
     cur.execute(update_sql);
     
     conn.commit()
     print(cur.rowcount,"record is updated")
     
#update()

#to delete record
def delete():
     conn = pymysql.connect(host = "localhost",user = "root", password = "", db = "student");
     
     roll_no = int(input("Enter roll number:"))
     
     #delete_sql = "DELETE FROM `1st_year` WHERE `1st_year`.`roll_no` = '"+str(roll_no)+"' ";
     delete_sql = f"DELETE FROM `1st_year` WHERE `1st_year`.`roll_no` = '{roll_no}' ";
     cur = conn.cursor();
     cur.execute(delete_sql);
     
     conn.commit()
     print(cur.rowcount,"record is deleted")
     
#delete()

ch = 10
print("Welcome to python CRUD application")

while ch != 0:
    print("1.List of all records \n2.Insert \n3.Update \n4.Delete \n0.Exit")
    
    ch = int(input("Enter your choice: "))
    
    if ch == 1:
        showrecord()
        
    if ch == 2:
        insert()
        showrecord()
        
    if ch == 3:
        update()
        showrecord()
        
    if ch == 4:
        delete()
        showrecord()

