from flask import Flask, jsonify, request
from flask_cors import CORS
import pymysql
app = Flask(__name__)
cors = CORS(app)


@app.route('/users', methods=['GET'])
def get_users():
    # To connect MySQL database
    conn = pymysql.connect(host='sql6.freesqldatabase.com', user='sql6468115', password = '5TzhdHvzpL', db='sql6468115')

    cur = conn.cursor()
    cur.execute("SELECT * FROM 1st_year")
    output = cur.fetchall()


    for rec in output:
        print(rec);

    # To close the connection
    conn.close()

    return jsonify(output);

@app.route('/users', methods=['DELETE'])
def deleteRecord():
    # To connect MySQL database
    conn = pymysql.connect(host='sql6.freesqldatabase.com', user='sql6468115', password = '5TzhdHvzpL', db='sql6468115')
    cur = conn.cursor()
    roll_no = int(request.args.get('roll_no'));

    query = f"Delete FROM 1st_year WHERE roll_no ={roll_no}";
    res = cur.execute(query);
    conn.commit();
    print(cur.rowcount,"record(s) deleted");

    return {"result" : "Record deleted Succesfully"}

@app.route('/users', methods=['POST'])
def insertRecord():
    conn= pymysql.connect(host='sql6.freesqldatabase.com',user='sql6468115',password= '5TzhdHvzpL',db='sql6468115')

    #get raw json values
    raw_json = request.get_json();
    name = raw_json["name"];
    section = raw_json["section"];
    city = raw_json["city"];
    sql=f"INSERT INTO 1st_year (roll_no,name,section,city) VALUES (NULL,'{name}','{section}','{city}')";
    print(sql);
    cur= conn.cursor();

    cur.execute(sql);
    conn.commit();
    return {"result" : "Record inserted Succesfully"}

@app.route('/users', methods=['PUT'])
def updateRecord():
    conn= pymysql.connect(host='sql6.freesqldatabase.com',user='sql6468115',password= '5TzhdHvzpL',db='sql6468115')

    raw_json = request.get_json();

    #print(type(raw_json));
    roll_no = raw_json['roll_no'];
    name = raw_json["name"];
    section = raw_json["section"];
    city = raw_json["city"];
    sql_update_query=(f"UPDATE sql6468115.1st_year SET name = '{name}', section = '{section}', city = '{city}' WHERE roll_no = '{roll_no}'");
    cur = conn.cursor()
    cur.execute(sql_update_query);
    conn.commit()
    return {"result" : "Record updated Succesfully"}


if __name__ == "__main__":  #it will generate port number
    app.run(debug=True);
